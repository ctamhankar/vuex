import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import App from './App.vue';
import { routes } from './routes';
import {ADD_PRODUCT_TO_CART, CHECKOUT, INCREASE_PRODUCT_QUANTITY, UPDATE_COUPON_CODE} from './mutation-types';

Vue.filter('currency', function(value) {
    let formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0
    });
    
    return formatter.format(value);
});

Vue.use(Vuex);
Vue.use(VueResource);
Vue.use(VueRouter);

const router = new VueRouter({
    routes: routes,
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        if (to.hash) {
            return {
                selector: to.hash
            };
        }
        
        if (savedPosition) {
            return savedPosition;
        }
        
        return { x: 0, y: 0 };
    }
});

const store = new Vuex.Store({
    state: {
        cart: {
            items: []
        },
        couponCode: ''
    },
    actions: {
        [ADD_PRODUCT_TO_CART] ({ commit, getters }, payload) {
            return new Promise((resolve, reject) => {
                let cartItem = getters.getCartItem(payload.product);
                payload.cartItem = cartItem;
                
                if(cartItem == null) {
                    let requestUrl = 'http://localhost:3000/cart/add/{productId}/{quantity}';
                    Vue.http.post(requestUrl, {}, {
                        params: {
                            productId: payload.product.id,
                            quantity: payload.quantity
                        }
                    }).then(
                        response => {
                            commit(ADD_PRODUCT_TO_CART, payload)
                            resolve();
                        },
                        response => {
                            alert("Could not add product to cart");
                            reject();
                        }
                    );
                }
                else {
                    let requestUrl = 'http://localhost:3000/cart/increase-quantity/{productId}';
                    Vue.http.post(requestUrl, {}, {
                        params: {
                            productId: payload.product.id
                        }
                    }).then(
                        response => {
                            commit(INCREASE_PRODUCT_QUANTITY, payload);
                            resolve();
                        },
                        response => {
                            alert("Could not increase quantity for the product");
                            reject();
                        }
                    );
                }
            });
            
            /*context.commit(ADD_PRODUCT_TO_CART, payload);*/
            
        },
        [CHECKOUT] (context, payload) {
            /*let requestUrl = 'http://localhost:3000/cart/add/{productId}/{quantity}';
            let cartItems = this.state.cart.items;
            this.state.cart.items.forEach(function(item) {
                Vue.http.post(requestUrl, {}, {
                    params: {
                        productId: item.product.id,
                        quantity: -item.quantity
                    }
                }).then(
                    response => {
                        if(item.product.id === cartItems[cartItems.length - 1].product.id) {
                            context.commit(CHECKOUT, payload)
                        }
                    },
                    response => alert("Could not add product to cart")
                );
            });*/
            let requestUrl = 'http://localhost:3000/cart/done';
            Vue.http.post(requestUrl, this.state.cart.items, {
                params: {
                    
                }
            }).then(
                response => context.commit(CHECKOUT, payload),
                response => alert("Could not add product to cart")
            );
            /*context.commit(CHECKOUT, payload);*/
        }
    },
    getters: {
        cartTotal: (state) => {
            let total = 0;

            state.cart.items.forEach(function(item){
                total += item.product.price * item.quantity;
            });

            //console.log("total", total);

            return total;
        },
        /*taxAmount: (state, getters) => {
            return function(percentage) {
                return ((getters.cartTotal * percentage) / 100);
            };
        }*/
        taxAmount: (state, getters) => (percentage) =>{
            return ((getters.cartTotal * percentage) / 100);
        },
        getCartItem: (state) => (product) => {
            for(let i = 0; i < state.cart.items.length; i++) {
                if(state.cart.items[i].product.id === product.id) {
                    return state.cart.items[i];
                }
            }

            return null;
        }
    },
    mutations: {
        [CHECKOUT] (state) {
            state.cart.items.forEach(function(item) {
                item.product.inStock += item.quantity;
            });
            
            state.cart.items = [];
        },
        [ADD_PRODUCT_TO_CART] (state, payload) {
            //let cartItem = state.getCartItem(payload.product);
            /*if (payload.cartItem != null) {
                payload.cartItem.quantity += payload.quantity;
            } else {
                state.cart.items.push({
                    product: payload.product,
                    quantity: payload.quantity
                });
            }*/
            state.cart.items.push({
                product: payload.product,
                quantity: payload.quantity
            });
            payload.product.inStock -= payload.quantity;
        },
        [INCREASE_PRODUCT_QUANTITY] (state, payload) {
            payload.cartItem.quantity += payload.quantity;
            payload.product.inStock -= payload.quantity;
        },
        [UPDATE_COUPON_CODE] (state, payload) {
            state.couponCode = payload;
        }
    }
});

Vue.http.options.root = 'http://localhost:3000';

new Vue({
    el: '#app',
    render: h => h(App),
    router: router,
    store: store
});